//
//  SettingsViewController.swift
//  frittie
//
//  Created by Duc Vu on 11/7/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import UIKit

class SettingsViewController: TabPageViewController {
  
  @IBOutlet weak var nameLabel: UILabel!
  
  @IBOutlet weak var userAvatar: UIImageView!
  
  @IBOutlet weak var settingsButton: UIButton!
  
  @IBOutlet weak var signoutBtn: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    nameLabel.text = StateManager.sharedInstance.accountInfo.firstname + " " + StateManager.sharedInstance.accountInfo.lastname
    signoutBtn.addTarget(self, action: "signout:", forControlEvents: UIControlEvents.TouchUpInside)
  }
  
  func signout(sender:UIButton!){
    InteractionManager.sharedInstance.navController.popViewControllerAnimated(true)
  }
}
