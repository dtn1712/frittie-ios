//
//  InteractionManager.swift
//  frittie
//
//  Created by Hao Wu on 30/10/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import Foundation
import UIKit
class InteractionManager: NSObject {
  var navController = UINavigationController()
  //global activityIndicator share for every view
  var activityIndicator : UIActivityIndicatorView!
  class var sharedInstance : InteractionManager {
    struct Static {
      static let instance : InteractionManager = InteractionManager()
    }
    return Static.instance
  }
  func initializeNavigation(){
    if StateManager.sharedInstance.accountInfo.userToken == "" {
      let loginVC = SignInViewController(nibName: "SignInViewController", bundle: nil)
      navController.pushViewController(loginVC, animated: false)
    } else {
      let tbc = MainTabBarController()
      navController.setViewControllers([tbc], animated: false)
    }
  }
  
  //start activityIndicator
  func startIndicator (parentView:UIView){
    activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0,0, 80, 80)) as UIActivityIndicatorView
    activityIndicator.center = parentView.center
    activityIndicator.layer.cornerRadius = 20
    activityIndicator.hidesWhenStopped = true
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
    parentView.addSubview(activityIndicator)
    dispatch_async(dispatch_get_main_queue(), {
      self.activityIndicator.startAnimating()
    });
  }
  
  //stop activityIndicator
  func stopIndicator(){
    dispatch_async(dispatch_get_main_queue(), {
      self.activityIndicator.stopAnimating()
    });
  }
}