//
//  Constants.swift
//  frittie
//
//  Created by Hao Wu on 30/10/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import Foundation

let SETTING_VIEW_NAME: String = "Settings"
let EXPLORE_VIEW_NAME: String = "Explore"
let ACTIVITY_VIEW_NAME: String = "Activities"
let API_DOMAIN: String = "http://www.beta.frittie.com/api/frittie"