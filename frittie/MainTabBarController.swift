//
//  MainTabBarController.swift
//  frittie
//
//  Created by Zichen Zheng on 10/29/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // prepare location page
    let exploreViewController = ExploreViewController(nibName: "ExploreViewController", bundle: nil)
    exploreViewController.tabBarItem.image = UIImage(named: "second")
    exploreViewController.tabBarItem.title = EXPLORE_VIEW_NAME
  
    // prepare activity page
    let activityViewController = ActivityViewController(nibName: "ActivityViewController", bundle: nil)
    activityViewController.tabBarItem.image = UIImage(named: "first")
    activityViewController.tabBarItem.title = ACTIVITY_VIEW_NAME
    
    // prepare explore page
    let settingsViewController = SettingsViewController(nibName: "SettingsViewController", bundle: nil)
    settingsViewController.tabBarItem.image = UIImage(named: "first")
    settingsViewController.tabBarItem.title = SETTING_VIEW_NAME
    
    // add view controllers to tab bar
    setViewControllers([exploreViewController, activityViewController, settingsViewController], animated: false)
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}
