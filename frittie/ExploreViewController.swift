//
//  LocationViewController.swift
//  frittie
//
//  Created by Zichen Zheng on 10/29/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import UIKit
import MapKit
import MapKit
import CoreLocation
import Alamofire

class ExploreViewController: TabPageViewController, MKMapViewDelegate, CLLocationManagerDelegate  {
  @IBOutlet var mapView: MKMapView!
  var locManager: CLLocationManager?
  var lastLocation: CLLocation!
  override func viewDidLoad() {
    super.viewDidLoad()
    var pointAnnotation:MKPointAnnotation = MKPointAnnotation()
    dispatch_async(dispatch_get_main_queue(), {
      self.lastLocation = CLLocation(latitude: 37.7853889, longitude: -122.4056973)
    })
    self.mapView.delegate = self
    self.locManager = CLLocationManager()
    self.locManager!.delegate = self;
    if (!CLLocationManager.locationServicesEnabled()) {
      println("Location services are not enabled")
    }
    self.locManager!.requestAlwaysAuthorization()
    self.locManager!.startUpdatingLocation()
  }
  
  // override function to set up customizedPin
  func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
    if (annotation.isKindOfClass(MKUserLocation)){
      return nil
    }
    var customizedPin = mapView.dequeueReusableAnnotationViewWithIdentifier("MyIdentifier") as? MKPinAnnotationView
    if customizedPin != nil {
      return customizedPin
    }
    customizedPin = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "MyIdentifier")
    customizedPin?.pinColor = .Green
    if (annotation.title == "You are here"){
      customizedPin?.image = UIImage(named: "icn_pin")
    } else {
      customizedPin?.image = UIImage(named: "icn_expand")
    }
    customizedPin?.canShowCallout = true
    return customizedPin
  }
  
  // override function to set mapview to user current location
  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    let location = locations.last as CLLocation
    if (location.coordinate.latitude != lastLocation.coordinate.latitude && location.coordinate.longitude != lastLocation.coordinate.longitude){
      let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
      let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03))
      mapView.setRegion(region, animated: true)
      var currentPointAnnotation:MKPointAnnotation = MKPointAnnotation()
      currentPointAnnotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
      currentPointAnnotation.title = "You are here"
      self.mapView.delegate = self
      mapView.addAnnotation(currentPointAnnotation)
      getLocationAround(location)
    }
    lastLocation = location
  }
  
  func getLocationAround(location:CLLocation){
    let parameters = [
      "lat": location.coordinate.latitude,
      "lng": location.coordinate.longitude,
      "rad": 40
    ]
    var nearbyLocations:NSMutableArray!
    InteractionManager.sharedInstance.startIndicator(self.view)
    var listAnnotations: NSMutableArray = []
    var deserializelistLocation: NSMutableArray = []
    var request: String =  "/Location/d/nearby?username="+StateManager.sharedInstance.accountInfo.username+"&api_key="+StateManager.sharedInstance.accountInfo.userToken
    Alamofire.request(.POST, API_DOMAIN + request, parameters: parameters)
      .responseJSON { (request, response, JSON, error) in
        nearbyLocations = JSON as NSMutableArray
        var i : NSDictionary?
        for i in nearbyLocations{
          deserializelistLocation.addObject(i.valueForKey("bundle") as NSMutableDictionary)
        }
        for j in deserializelistLocation{
          var lat :NSString = j.valueForKey("lat") as NSString
          var lng :NSString = j.valueForKey("lng") as NSString
          var pointAnnotation:MKPointAnnotation = MKPointAnnotation()
          pointAnnotation.coordinate = CLLocationCoordinate2DMake(lat.doubleValue, lng.doubleValue)
          pointAnnotation.title = j.valueForKey("name") as NSString
          self.mapView.addAnnotation(pointAnnotation)
        }
        InteractionManager.sharedInstance.stopIndicator()
    }
  }
  
}
