//
//  LogInViewController.swift
//  frittie
//
//  Created by Duc Vu on 11/4/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import UIKit
import Alamofire
class SignInViewController: UIViewController {
  @IBOutlet weak var signinBtn: UIButton!
  @IBOutlet weak var emailInput: UITextField!
  @IBOutlet weak var passwordInput: UITextField!

  @IBOutlet weak var signupBtn: UIButton!
  override func viewDidLoad() {
    super.viewDidLoad()
    signinBtn.addTarget(self, action: "signIn:", forControlEvents: UIControlEvents.TouchUpInside)
    signupBtn.addTarget(self, action: "signUp:", forControlEvents: UIControlEvents.TouchUpInside)  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func signIn(sender:UIButton!){
    let parameters = [
      "email": String(emailInput.text),
      "password": String(passwordInput.text),
    ]
    var info:NSDictionary!
    println(emailInput.text)
    InteractionManager.sharedInstance.startIndicator(self.view)
    Alamofire.request(.POST, API_DOMAIN + "/User/signin", parameters: parameters)
      .responseJSON { (request, response, JSON, error) in
        println(JSON)
        if (JSON != nil){
          info = JSON as NSDictionary
          var success =  info["success"] as Int
          if ( success  == 1){
            let tbc = MainTabBarController()
            tbc.navigationItem.leftBarButtonItem = nil
            StateManager.sharedInstance.signin(info["username"] as String,userToken: info["api_key"] as String, firstname: info["first_name"] as String, lastname: info["last_name"] as String)
            InteractionManager.sharedInstance.stopIndicator()
            InteractionManager.sharedInstance.navController.popViewControllerAnimated(true)
            InteractionManager.sharedInstance.navController.pushViewController(tbc, animated: false)
          } else {
          }
        }
    }
  }
  
  func signUp(sender:UIButton!){
    let signupVC = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
    InteractionManager.sharedInstance.navController.pushViewController(signupVC, animated: false)
  }
}
