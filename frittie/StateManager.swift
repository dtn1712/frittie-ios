//
//  StateManager.swift
//  frittie
//
//  Created by Wu Hao on 30/10/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import Foundation

class StateManager: NSObject {
  var accountInfo = AccountInformation()
  class var sharedInstance : StateManager {
  struct Static {
    static let instance : StateManager = StateManager()
    }
    return Static.instance
  }
  
  
  func signin(username: String, userToken: String, firstname: String, lastname: String){
    //let dictionary = JSONParseDictionary(string);
    accountInfo.userToken = userToken
    accountInfo.lastname = lastname
    accountInfo.firstname = firstname
    accountInfo.username = username
  }
  
  func signout(){
    accountInfo.userToken = ""
    accountInfo.lastname = ""
    accountInfo.firstname = ""
    accountInfo.username = ""
  }

}