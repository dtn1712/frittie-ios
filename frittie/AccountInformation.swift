//
//  AccountInformation.swift
//  frittie
//
//  Created by Duc Vu on 11/5/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import Foundation

class AccountInformation: NSObject {
  var userToken: String = ""
  var username: String = ""
  var firstname: String = ""
  var lastname: String = ""
}