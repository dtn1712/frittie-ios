//
//  TabPageViewController.swift
//  frittie
//
//  Created by Zichen Zheng on 11/2/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import UIKit

class TabPageViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    self.tabBarController?.navigationItem.title = tabBarItem.title
  }
  
}
