//
//  ActivityViewController.swift
//  frittie
//
//  Created by Hao Wu on 30/10/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import UIKit

class ActivityViewController: TabPageViewController {
  
  @IBOutlet var segmentControl: UISegmentedControl!
  
  @IBAction func segmentControlAction(sender: AnyObject) {
    if (segmentControl.selectedSegmentIndex == 0) {
      // Description
      
    } else if (segmentControl.selectedSegmentIndex == 1) {
      // Participants
      
    } else if (segmentControl.selectedSegmentIndex == 2) {
      // Comments
        
    }
  }
  
}