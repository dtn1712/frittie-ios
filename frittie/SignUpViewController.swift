//
//  SignUpViewController.swift
//  frittie
//
//  Created by Duc Vu on 11/6/14.
//  Copyright (c) 2014 Frittie. All rights reserved.
//

import UIKit
import Alamofire

class SignUpViewController: UIViewController {


  
  @IBOutlet weak var emailInput: UITextField!
  @IBOutlet weak var passwordInput: UITextField!
  
  @IBOutlet weak var firstnameInput: UITextField!
  
  @IBOutlet weak var lastnameInput: UITextField!
  
  @IBOutlet weak var submitBtn: UIButton!
  override func viewDidLoad() {
        super.viewDidLoad()
      submitBtn.addTarget(self, action: "register:", forControlEvents: UIControlEvents.TouchUpInside)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
   func register(sender:UIButton!){
    let parameters = [
      "email": String(emailInput.text),
      "password": String(passwordInput.text),
      "first_name": String(firstnameInput.text),
      "last_name": String(lastnameInput.text)
    ]
    InteractionManager.sharedInstance.startIndicator(self.view)
    var info:NSDictionary!
    println(emailInput.text)
    Alamofire.request(.POST, API_DOMAIN + "/User/signup", parameters: parameters)
      .responseJSON { (request, response, JSON, error) in
        println(JSON)
        if (JSON != nil){
          info = JSON as NSDictionary
          println(JSON)
          var success =  info["success"] as Int
          if ( success  == 1){
            InteractionManager.sharedInstance.stopIndicator()
            InteractionManager.sharedInstance.navController.popViewControllerAnimated(true)
          } else {
          }
        }
    }
  }

}
